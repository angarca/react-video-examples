import React, { Component } from 'react';
import './example1.css';

class Example1 extends Component {
  constructor( props) {
    super( props);
    this.state = {
      animation: [],
      clases: [],
      interactivePosition: false,
      position: {
        x: window.innerWidth / 2 - 25,
        y: window.innerHeight / 2 - 25 * 9 / 16,
      },
    };
    this.onTime = this.onTime.bind( this);
    this.onKeyDown = this.onKeyDown.bind( this);
  }
  componentWillMount() {
    fetch( '/player-animation.json').then( response => response.json()).then( response => {
      this.setState({ animation: response});
    });
    document.getElementsByTagName('body')[0].onkeydown = this.onKeyDown;
  }
  componentWillUpdate() {
  }
  onKeyDown( event) {
    let position = this.state.position;
    switch( event.key) {
      case 'ArrowLeft':
        if( position.x - 10 >= 1) position.x -= 10;
        break;
      case 'ArrowUp':
        if( position.y - 10 >= 1) position.y -= 10;
        break;
      case 'ArrowRight':
        if( position.x + 10 <= window.innerWidth - 50) position.x += 10;
        break;
      case 'ArrowDown':
        if( position.y + 10 <= window.innerHeight - 50 * 9 / 16) position.y += 10;
        break;
      default:;
    }
    this.setState({ position})
  }
  onTime( event) {
    let clases = [];
    let interactivePosition = false;
    this.state.animation.forEach( state => {
      if( state.start <= event.target.currentTime && state.end >= event.target.currentTime) {
        clases.push( state.class);
        if( state.position === 'interactive') {
          interactivePosition = true;
        }
      }
    });
    this.setState({ clases, interactivePosition});
  }
  render() {
    const clases = this.state.clases.join(' ');
    const style = this.state.interactivePosition ? { left: this.state.position.x, top: this.state.position.y} : {};
    return (
      <video
        className={'player ' + clases}
        controls
        autoPlay
        loop
        onTimeUpdate={this.onTime}
        src='//vr.jwplayer.com/content/AgqYcfAT/AgqYcfAT-8yQ1cYbs.mp4'
        style={style}
      />
    );
  }
}

export default Example1;
