import React, { Component } from 'react';
import Example1 from './example1';
import Example2 from './example2';

const examples = {
  example1: <Example1/>,
  example2: <Example2/>,
}

class App extends Component {
  constructor( props) {
    super( props);
    this.renderButton = this.renderButton.bind( this);
    this.state = {
      active: Object.keys( examples)[0],
    };
  }
  renderButton( key) {
    return <button key={key} onClick={() => this.setState({ active: key})}>{ key }</button>;
  }
  render() {
    return (
      <div>
        <div>
          { Object.keys( examples).map( this.renderButton)}
        </div>
        <div>
          { examples[ this.state.active ]}
        </div>
      </div>
    );
  }
}

export default App;
