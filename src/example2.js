import React, { Component } from 'react';
import './example2.css';

class Example2 extends Component {
  constructor( props) {
    super( props);
    this.state = {
      videos: {},
      currentTime: 0,
    };
    this.populate = this.populate.bind( this);
    this.clean = this.clean.bind( this);
    this.update = this.update.bind( this);
    this.onTime = this.onTime.bind( this);
    this.renderVideo = this.renderVideo.bind( this);
    this.lastKey = -1;
  }
  populate() {
    this.lastKey += 1;
    const width = Math.random() * 150 + 150;
    const left = Math.random() * ( window.innerWidth - width );
    let videos = {};
    videos[ this.lastKey] = {
      onEnded: () => delete this.state.videos[ this.lastKey],
      src: '//vr.jwplayer.com/content/AgqYcfAT/AgqYcfAT-8yQ1cYbs.mp4',
      style: { position: 'absolute', left, width},
      startTime: this.state.currentTime,
      velocity: Math.random() * 5,
      rotation: {
        x: Math.random(),
        y: Math.random(),
        z: Math.random(),
        angle: (Math.random() - 0.5) * 10,
      },
    }
    this.setState({ videos: Object.assign( videos, this.state.videos)});
  }
  clean() {
    let toClean = []
    Object.keys( this.state.videos).forEach( key => {
      const video = this.state.videos[ key];
      if( video.velocity * (this.state.currentTime - video.startTime) + video.style.width * 9 / 16 > window.innerHeight) {
        toClean.push( key);
      }
    });
    toClean.forEach( key => delete this.state.videos[ key]);
  }
  update() {
    this.setState({ currentTime: this.state.currentTime + 1});
  }
  onTime() {
    if( Object.keys( this.state.videos).length < 10) setTimeout( this.populate, 0);
    setTimeout( this.clean, 0);
    setTimeout( this.update, 0);
  }
  renderVideo( key, properties) {
    const time = this.state.currentTime - properties.startTime;
    const rotation = properties.rotation;
    return <video key={key} autoPlay onEnded={properties.onEnded} src={properties.src}
      style={ Object.assign( {}, properties.style, {
        top: time * properties.velocity,
        transform: 'rotate3d( ' + rotation.x + ',' + rotation.y + ',' + rotation.z + ',' + time * rotation.angle + 'deg)',
      }) }
    />;
  }
  render() {
    return (
      <div>
        { Object.keys( this.state.videos).map( key => this.renderVideo( key, this.state.videos[key]))}
      </div>
    );
  }
  componentDidMount() {
    setInterval( this.onTime, 50);
  }
}

export default Example2;
